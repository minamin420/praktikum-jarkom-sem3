# praktikum jarkom sem3

A program to change an alphabet into the next four.

Muhammad Amien Prananto 4210181027 <br>
Ilham Jalu Prakosa 4210181010 <br> <br>

Description :<br><br>

Client send an alphabet letter<br>
Server received the letter<br>
Server changed the letter<br>
Server send it back to client<br>
Client print the received letter that has been changed
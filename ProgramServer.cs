﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;
using System.Threading;

namespace Server
{
    class Program
    {
        private static string message = string.Empty;
        private static TcpListener tcpListener;
        private static List<TcpClient> tcpClientsList = new List<TcpClient>();

        static void Main(string[] args)
        {
            tcpListener = new TcpListener(IPAddress.Parse("127.0.0.1"), 8080);
            tcpListener.Start();

            Console.WriteLine("Server started");

            while (true)
            {
                TcpClient tcpClient = tcpListener.AcceptTcpClient();
                tcpClientsList.Add(tcpClient);

                Thread thread = new Thread(ClientListener);
                thread.Start(tcpClient);

            }
        }

        public static void ClientListener(object obj)
        {
            TcpClient tcpClient = (TcpClient)obj;
            StreamReader reader = new StreamReader(tcpClient.GetStream());

            Console.WriteLine("Client connected");


            while (true)
            {
                message = reader.ReadLine();
                BroadCast(message, tcpClient);
                Console.WriteLine(message);

                if (message == "a")
                {
                    StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());
                    sWriter.WriteLine("e");
                    sWriter.Flush();
                }


                else if (message == "b")
                {
                    StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());
                    sWriter.WriteLine("f");
                    sWriter.Flush();
                }


                else if (message == "c")
                {
                    StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());
                    sWriter.WriteLine("g");
                    sWriter.Flush();
                }

                else if (message == "d")
                {
                    StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());
                    sWriter.WriteLine("h");
                    sWriter.Flush();
                }

                else if (message == "e")
                {
                    StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());
                    sWriter.WriteLine("i");
                    sWriter.Flush();
                }

                else if (message == "f")
                {
                    StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());
                    sWriter.WriteLine("j");
                    sWriter.Flush();
                }

                else if (message == "g")
                {
                    StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());
                    sWriter.WriteLine("k");
                    sWriter.Flush();
                }

                else if (message == "h")
                {
                    StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());
                    sWriter.WriteLine("l");
                    sWriter.Flush();
                }

                else if (message == "i")
                {
                    StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());
                    sWriter.WriteLine("m");
                    sWriter.Flush();
                }

                else if (message == "j")
                {
                    StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());
                    sWriter.WriteLine("n");
                    sWriter.Flush();
                }

                else if (message == "k")
                {
                    StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());
                    sWriter.WriteLine("o");
                    sWriter.Flush();
                }

                else if (message == "l")
                {
                    StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());
                    sWriter.WriteLine("p");
                    sWriter.Flush();
                }

                else if (message == "m")
                {
                    StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());
                    sWriter.WriteLine("q");
                    sWriter.Flush();
                }

                else if (message == "n")
                {
                    StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());
                    sWriter.WriteLine("r");
                    sWriter.Flush();
                }

                else if (message == "o")
                {
                    StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());
                    sWriter.WriteLine("s");
                    sWriter.Flush();
                }

                else if (message == "p")
                {
                    StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());
                    sWriter.WriteLine("t");
                    sWriter.Flush();
                }

                else if (message == "q")
                {
                    StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());
                    sWriter.WriteLine("u");
                    sWriter.Flush();
                }

                else if (message == "r")
                {
                    StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());
                    sWriter.WriteLine("v");
                    sWriter.Flush();
                }

                else if (message == "s")
                {
                    StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());
                    sWriter.WriteLine("w");
                    sWriter.Flush();
                }

                else if (message == "t")
                {
                    StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());
                    sWriter.WriteLine("x");
                    sWriter.Flush();
                }

                else if (message == "u")
                {
                    StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());
                    sWriter.WriteLine("y");
                    sWriter.Flush();
                }

                else if (message == "v")
                {
                    StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());
                    sWriter.WriteLine("z");
                    sWriter.Flush();
                }

                else if (message == "w")
                {
                    StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());
                    sWriter.WriteLine("a");
                    sWriter.Flush();
                }

                else if (message == "x")
                {
                    StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());
                    sWriter.WriteLine("b");
                    sWriter.Flush();
                }

                else if (message == "y")
                {
                    StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());
                    sWriter.WriteLine("c");
                    sWriter.Flush();
                }

                else if (message == "z")
                {
                    StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());
                    sWriter.WriteLine("d");
                    sWriter.Flush();
                }
            }
        }


        public static void BroadCast(string msg, TcpClient excludeClient)
        {
            foreach (TcpClient client in tcpClientsList)
            {
                if (client != excludeClient)
                {
                    StreamWriter sWriter = new StreamWriter(client.GetStream());
                    sWriter.WriteLine(msg);
                    sWriter.Flush();

                }

            }
        }
    }
}